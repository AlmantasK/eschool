package School;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

public class PersonTests {
    // act given when then
    //
    @Test
    void getAge_when_person_bornToday_returns_0(){
        // Arrange
        Person person = new Person("a", "a", LocalDate.now(), 0, 1, Gender.MALE);

        // Act
        int age = person.getAge();

        // Assert
        assertThat(age).isEqualTo(0);
    }

    @Test
    void new_given_nullFirstName_throws_IllegalArgumentException(){
        // Arrange
        String firstName = null;

        // Act
        Throwable thrown =  catchThrowable(() -> buildPersonWithFirstName(firstName));

        // Assert
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("firstName");
    }

    private static Person buildPersonWithFirstName(String firstName){
        return new Person(firstName, "a", LocalDate.now(), 1, 1, Gender.MALE);
    }
}
