package Generics;

public class CupOfCoffee extends Cup<Coffee> {

    public CupOfCoffee(Coffee coffee) {
        super(coffee);
    }
}
