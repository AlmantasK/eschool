package Generics;

public class Cup<T> {
    public T Something;

    public Cup(T something) {
        Something = something;
    }

    public void fill(T Something){
        System.out.println("filling with " + Something.getClass().getName());
    }

    public void drink(){
        System.out.println("drinking " + Something.getClass().getName());
    }
}
